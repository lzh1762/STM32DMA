/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2021-03-19 10:28:58
 * @LastEditors: MichaelHu
 * @LastEditTime: 2021-03-19 10:28:58
 */
#ifndef __CRC32_H_
#define __CRC32_H_

unsigned int calc_crc32(unsigned int  crc, const void *buf, unsigned int size);
	
#endif

