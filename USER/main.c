/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2020-06-18 15:46:33
 * @LastEditors: MichaelHu
 * @LastEditTime: 2021-06-25 16:36:27
 */
#include "FIFO.h"
#include "delay.h"
#include "heap.h"
#include "usart.h"


//���Ͳ�������
//55 55 00 23 01 31 32 33 00 24 00 00 20 00 00 00 20 10 48 4F F4 20 51 42 07 06 D0 C0 1D 0D 49 20 F0 07 00 3F 22 03 53

int main(void)
{
    u8 res;
    u8 *buf;
    u8 *secondbuf;
    u16 secondLen;
    u16 totalLen;
    u8 cmd;
    delay_init();
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
    usart_dma_init(460800);
	
	//send_Serial_Data(1, "123", 200);
    for (;;)
    {
        res = data_Process(uartFIFO, &cmd, (u32 **)&buf, (u32 **)&secondbuf, &secondLen, &totalLen);
        if (res == OK)
        {
            if (cmd == 1)
            {
                if (secondLen > 0)
                {
                    for (int i = 0; i < (totalLen - secondLen); i++)
                    {
						while ((USART1->SR & 0X40) == 0) {};
						USART1->DR = (u8)buf[i];
                    }
                    for (int i = 0; i < secondLen; i++)
                    {
						while ((USART1->SR & 0X40) == 0) {};
						USART1->DR = (u8)secondbuf[i];
                    }
                }
                else
                {
					for (int i = 0; i < (totalLen - secondLen); i++)
					{
						while ((USART1->SR & 0X40) == 0) {};
						USART1->DR = (u8)buf[i];
					}
                }
            }
        }
        else if (res == CRC_NG)
        {
        }
    }
}
