#ifndef __USART_H
#define __USART_H
#include "stdio.h"	
#include "sys.h" 
#include "FIFO.h"

#define FIFO_BUF_LENGTH			1000	
#define FIFO_DMA		        1  //采用DMA模式

extern FIFOTYPE *uartFIFO;


#define uartCNDTR DMA1_Channel5->CNDTR


typedef enum
{
    OK = 1,
    CRC_NG,
	BUF_LENGTH_ERR,		//数据缓存长度不够
	NG,
} Process_RES_TypeDef;

void uart_init(u32 bound);
void send_Serial_Data(u8 cmd, u8 *buf, u16 length);
void send_Serial_CMD(u8 cmd);
void usart_dma_init(u32 bound);
Process_RES_TypeDef data_Process(FIFOTYPE *fifo, u8 *cmd, u32 **data, u32 **secondData, u16 *secondLen, u16 *totalLen);
#endif


